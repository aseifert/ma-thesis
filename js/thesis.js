function countWords(text) {
    var matches = text.match(/\S+\s*/g);
    return matches !== null ? matches.length : 0;
}

$(function () {
    $(document).ready(function() {
        var content = $('#content').text();
        var references = $('#references ~ div').text();
        var numWords = countWords(content) - countWords(references);

        $('body').append('<div id="top" style="text-align: center"><a href="#header"><i class="fa fa-angle-double-up fa-lg fa-border"></i></a> <br /><small>&asymp;' + Math.round(numWords*10/230)/10 + '&nbsp;pp. <br /><a href="./thesis.docx"><i class="fa fa-file-text"></i> .docx</a></small></div>');
    });

    $(document).scroll(function () {
      var currentHash = "header";

      $('h1, h2, h3, h4, h5').each(function () {
        var top = window.pageYOffset;
        var distance = top - $(this).offset().top;
        var hash = $(this).attr('id');

            if (distance < 30 && distance > -30 && currentHash != hash) {
                window.location.hash = (hash);
                currentHash = hash;

                // color the current element in the TOC
                $('#toc a').each(function () {
                  if ($(this).attr('href') == '#' + hash) {
                        // reset all link colors
                        $('#toc a').each(function () {
                            $(this).css('color', '#005498');
                        });
                        // color current element
                        $(this).css('color', '#ba3925');
                  }
                });
            }
        });
    });
});
