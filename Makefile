all: word html pdf

word: thesis.docx

html: thesis.html

html-no-ref: thesis-no-ref.html

pdf: html
	prince thesis/index.html -o thesis/thesis.pdf

thesis.docx: thesis.html
	pandoc --from html --to docx thesis/index.html --reference-docx src/reference.docx -o thesis/thesis.docx

thesis-no-ref.html:
	cp src/thesis.ad src/thesis-ref.ad
	cp css/*.css thesis/css/
	cp js/*.js thesis/js/
	asciidoctor -d book -a docinfo -a stylesheet=css/asciidoctor.css src/thesis-ref.ad -o thesis/index.html
	perl -i -p -e   's/\cite:[(\w+?),\s?(.+?)\]/(\1: \2)/g' thesis/index.html \
	&& perl -i -p -e 's/\[citenp:(\w+?),\s?(.+?)\]/\1: \2/g' thesis/index.html \
	&& perl -i -p -e   's/\cite:[(\w+?)\]/(\1)/g' thesis/index.html \
	&& perl -i -p -e 's/\[citenp:(\w+?)\]/\1/g' thesis/index.html
	rm src/*-ref.ad

thesis.html:
	cp css/*.css thesis/css/
	cp js/*.js thesis/js/
	asciidoctor -r asciidoctor-bibtex -d book -a docinfo -a stylesheet=css/asciidoctor.css src/thesis.ad -o thesis/index.html

clean:
	rm -f thesis/css/* thesis/js/* thesis/*.* src/*-ref.ad
